package ru.anenkov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.entity.User;

@FeignClient(name = "application-server-api")
@RequestMapping("/api/free")
public interface AuthRestFreeEndpoint {

    @Nullable
    @SneakyThrows
    @GetMapping("/users/{id}")
    public User findById(
            @PathVariable @Nullable final String id
    );

    @Nullable
    @SneakyThrows
    @GetMapping("/users")
    public Iterable<User> findAll();

    @SneakyThrows
    @GetMapping("/users/count")
    public long count();

}

