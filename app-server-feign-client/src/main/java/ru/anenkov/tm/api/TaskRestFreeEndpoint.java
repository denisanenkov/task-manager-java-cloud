package ru.anenkov.tm.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.dto.TaskDTO;

import java.util.List;

@FeignClient(name = "application-server-api")
@RequestMapping("/api/free")
interface TaskRestFreeEndpoint {
 

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TaskDTO> tasks();

    @GetMapping(value = "/tasks/{id}")
    TaskDTO getTask(
            @PathVariable @Nullable final String id
    );

    @PostMapping(value = "/tasks")
    TaskDTO addTask(@RequestBody @Nullable TaskDTO task);

    @PutMapping(value = "/tasks")
    TaskDTO updateTask(@RequestBody @Nullable TaskDTO task);

    @DeleteMapping(value = "/tasks/{id}")
    String deleteTask(
            @PathVariable @Nullable final String id
    );

    @DeleteMapping(value = "/tasks")
    void deleteAllTasks();

    @GetMapping(value = "/tasks/count")
    long count();

}
