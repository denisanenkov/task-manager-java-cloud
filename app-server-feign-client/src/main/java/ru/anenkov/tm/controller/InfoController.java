package ru.anenkov.tm.controller;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RefreshScope
@RestController
@RequestMapping("/testApi")
public class InfoController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET, produces = "application/json")
    public String hello() {
        return "hello";
    }

    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    public String data() {
        return new Date().toString();
    }

    @RequestMapping(value = "/sum", method = RequestMethod.GET, produces = "application/json")
    public String sum(
            @RequestParam(value = "a") Integer a,
            @RequestParam(value = "b") Integer b
    ) {
        return String.valueOf(a+b);
    }

}