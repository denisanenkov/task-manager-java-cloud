package ru.anenkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Task extends AbstractEntity {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date dateBegin;

    private Date dateFinish;

    private Project project;

    private User user;

    public Task(String name) {
        this.name = name;
    }

    public Task(Project project) {
        this.project = project;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

}
