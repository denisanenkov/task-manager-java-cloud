package ru.anenkov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString; ;
import ru.anenkov.tm.enumerated.Status;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Project extends AbstractEntity {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateBegin;

    private Date dateFinish;

    private User user;

    private List<Task> tasks;

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

}
