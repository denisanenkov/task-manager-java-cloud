package ru.anenkov.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.enumerated.Status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO extends AbstractEntityDTO {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateBegin;

    private Date dateFinish;

    private String userId;

    public ProjectDTO(String name) {
        this.name = name;
    }

    public ProjectDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(String name, String description, String userId) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    public ProjectDTO(String name, String description, Status status, Date dateBegin, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", dateBegin=" + dateBegin +
                ", dateFinish=" + dateFinish +
                '}';
    }

    public static ProjectDTO toProjectDTO(@NotNull final Project project) {
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setDateBegin(project.getDateBegin());
        projectDTO.setDateFinish(project.getDateFinish());
        return projectDTO;
    }

    public static List<ProjectDTO> toProjectListDTO(List<Project> projectList) {
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project : projectList) {
            projectDTOList.add(toProjectDTO(project));
        }
        return projectDTOList;
    }

}
