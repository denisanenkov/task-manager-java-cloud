package ru.anenkov.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.enumerated.Status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO extends AbstractEntityDTO {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date dateBegin;

    private Date dateFinish;

    private String projectId;

    private String userId;

    public TaskDTO(String name) {
        this.name = name;
    }

    public TaskDTO(String name, String description, Status status, Date dateBegin, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    public TaskDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(String name, String description, String projectId, String userId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TaskDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", dateBegin=" + dateBegin +
                ", dateFinish=" + dateFinish +
                ", projectId='" + projectId + '\'' +
                '}';
    }

    public static TaskDTO toTaskDTO(@NotNull final Task task) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setDateBegin(task.getDateBegin());
        taskDTO.setDateFinish(task.getDateFinish());
        if (task.getProject() != null) taskDTO.setProjectId(task.getProject().getId());
        return taskDTO;
    }

    public static List<TaskDTO> toTaskListDTO(List<Task> taskList) {
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task : taskList) {
            taskDTOList.add(toTaskDTO(task));
        }
        return taskDTOList;
    }

}
