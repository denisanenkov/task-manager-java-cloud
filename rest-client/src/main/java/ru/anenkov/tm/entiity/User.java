package ru.anenkov.tm.entiity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String id = UUID.randomUUID().toString();

    private String login;

    private String passwordHash;

    private String firstName = "";

    private String secondName = "";

    private String lastName = "";

    private List<Role> roles = new ArrayList<>();

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final String lastName
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "[USER" + "';\n" +
                "  ID = '" + id + "';\n" +
                ", LOGIN = '" + login + "';\n" +
                ", PASSWORD HASH = '" + passwordHash + "';\n" +
                ", FIRST NAME = " + firstName + "';\n" +
                ", SECOND NAME = " + secondName + "';\n" +
                ", LAST NAME = " + lastName + "';]\n\n";
    }

}