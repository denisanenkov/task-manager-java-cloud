package ru.anenkov.tm.communication;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.entiity.Task;

import java.util.List;

@Component
public class TaskCommunication {

    private final String URL = "http://localhost:8080/api/free";

    public static void main(String[] args) {
        TaskCommunication taskCommunication = new TaskCommunication();
        taskCommunication.addTask(new TaskDTO("WHERE", "I AM"));
        taskCommunication.addTask(new TaskDTO("WANT", "STILL"));
        taskCommunication.addTask(new TaskDTO("WEE", "HAPPENS"));
        System.out.println(taskCommunication.tasks());
    }

    public List<TaskDTO> tasks() {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<TaskDTO>> responseEntity =
                restTemplate.exchange(URL + "/tasks",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<TaskDTO>>() {
                        });
        @Nullable final List<TaskDTO> projectList = responseEntity.getBody();
        return projectList;
    }

    public TaskDTO getTaskById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<TaskDTO> responseEntity =
                restTemplate.exchange(URL + "/tasks/" + id,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<TaskDTO>() {
                        });
        TaskDTO taskDTO = responseEntity.getBody();
        return taskDTO;
    }

    public void deleteById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/tasks/" + id);
        System.out.println("Task with id \"" + id + "\" was deleted");
    }

    public void addTask(@Nullable final TaskDTO task) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        if (!isExists(task.getId())) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<TaskDTO> httpEntity = new HttpEntity<>(task, httpHeaders);
            restTemplate.exchange(URL + "/tasks", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<ProjectDTO>() {
                    });
        } else {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<TaskDTO> httpEntity = new HttpEntity<>(task, httpHeaders);
            restTemplate.exchange(URL + "/tasks", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<ProjectDTO>() {
                    });
        }
    }

    public void deleteAllTasks() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/tasks");
    }

    public boolean isExists(@Nullable final String id) {
        @Nullable final List<TaskDTO> tasks = tasks();
        for (TaskDTO currentTask : tasks) {
            if (id.equals(currentTask.getId())) return true;
        }
        return false;
    }

}
