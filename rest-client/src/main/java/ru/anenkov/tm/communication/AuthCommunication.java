package ru.anenkov.tm.communication;

import org.jetbrains.annotations.Nullable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.entiity.User;

import java.util.List;

public class AuthCommunication {

    private final String URL = "http://localhost:8080/api/free";

    @Nullable
    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public static void main(String[] args) {
        AuthCommunication authCommunication = new AuthCommunication();
        System.out.println(authCommunication.getUserById("b72601a9-8a28-4362-b484-83bf1584d118").getId());
    }

    public List<User> users() {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        @Nullable ResponseEntity<List<User>> responseEntity =
                restTemplate.exchange(URL + "/users",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<User>>() {
                        });
        @Nullable final List<User> userList = responseEntity.getBody();
        return userList;

    }

    public User getUserById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        @Nullable ResponseEntity<User> responseEntity =
                restTemplate.exchange(URL + "/users/" + id,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<User>() {
                        });
        @Nullable final User userDTO = responseEntity.getBody();
        return userDTO;
    }

    public void addUser(@Nullable final User user) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        if (!isExists(user.getId())) {
            @Nullable HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            @Nullable HttpEntity<User> httpEntity = new HttpEntity<>(user, httpHeaders);
            restTemplate.exchange(URL + "/users", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<User>() {
                    });
        } else {
            @Nullable HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            @Nullable HttpEntity<User> httpEntity = new HttpEntity<>(user, httpHeaders);
            restTemplate.exchange(URL + "/users", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<User>() {
                    });
        }
    }

    public void deleteUserById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/users/" + id);
        System.out.println("User with id \"" + id + "\" was deleted");
    }

    public void deleteAllUsers() {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/users");
    }

    public boolean isExists(@Nullable final String id) {
        @Nullable final List<User> userList = users();
        for (User currentUser : userList) {
            if (id.equals(currentUser.getId())) return true;
        }
        return false;
    }

}
