package ru.anenkov.tm.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Component;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import org.springframework.http.*;

import java.util.List;

@Component
public class ProjectCommunication {

    private final String URL = "http://localhost:8080/api/free";

    public static void main(String[] args) {
        ProjectCommunication projectCommunication = new ProjectCommunication();
//        projectCommunication.addProject(new ProjectDTO("WHERE", "I AM"));
//        projectCommunication.addProject(new ProjectDTO("WANT", "STILL"));
//        projectCommunication.addProject(new ProjectDTO("WEE", "HAPPENS"));
        System.out.println(projectCommunication.projects());
    }

    public List<ProjectDTO> projects() {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        @Nullable ResponseEntity<List<ProjectDTO>> responseEntity =
                restTemplate.exchange(URL + "/projects",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<ProjectDTO>>() {
                        });
        @Nullable final List<ProjectDTO> projectList = responseEntity.getBody();
        return projectList;

    }

    public ProjectDTO getProjectById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        @Nullable ResponseEntity<ProjectDTO> responseEntity =
                restTemplate.exchange(URL + "/projects/" + id,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<ProjectDTO>() {
                        });
        @Nullable final ProjectDTO projectDTO = responseEntity.getBody();
        return projectDTO;
    }

    public void addProject(@Nullable final ProjectDTO project) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        if (!isExists(project.getId())) {
            @Nullable HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            @Nullable HttpEntity<ProjectDTO> httpEntity = new HttpEntity<>(project, httpHeaders);
            restTemplate.exchange(URL + "/projects", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<ProjectDTO>() {
                    });
        } else {
            @Nullable HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            @Nullable HttpEntity<ProjectDTO> httpEntity = new HttpEntity<>(project, httpHeaders);
            restTemplate.exchange(URL + "/projects", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<ProjectDTO>() {
                    });
        }
    }

    public void deleteProjectById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/projects/" + id);
        System.out.println("Project with id \"" + id + "\" was deleted");
    }

    public void deleteAllProjects() {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/projects");
    }

    public boolean isExists(@Nullable final String id) {
        @Nullable final List<ProjectDTO> projectList = projects();
        for (ProjectDTO currentProject : projectList) {
            if (id.equals(currentProject.getId())) return true;
        }
        return false;
    }

}
