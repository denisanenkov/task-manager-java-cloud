package ru.anenkov.tm.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.anenkov.tm.communication.TaskCommunication;
import ru.anenkov.tm.configuration.MyConfiguration;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.entiity.Task;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

public class TaskClient {

    private static ApplicationContext context =
            new AnnotationConfigApplicationContext(MyConfiguration.class);
    private static TaskCommunication communication = context.getBean("taskCommunication", TaskCommunication.class);

    public List<TaskDTO> getTaskList() {
        List<TaskDTO> tasks = communication.tasks();
        System.out.println(tasks);
        return tasks;
    }

    public TaskDTO getTaskById() {
        String id = "";
        System.out.print("ENTER TASK ID: ");
        id = TerminalUtil.nextLine();
        TaskDTO task = communication.getTaskById(id);
        System.out.println(task);
        return task;
    }

    public void deleteTask() {
        System.out.print("ENTER TASK ID: ");
        String id = TerminalUtil.nextLine();
        communication.deleteById(id);
    }

    public void addTask() {
        TaskDTO firstTask = new TaskDTO("first some name", "some description");
        TaskDTO secondTask = new TaskDTO("second some name", "some description");
        secondTask.setId(firstTask.getId());
        communication.addTask(firstTask);
        communication.addTask(secondTask);
    }

    public static void main(String[] args) {
        TaskClient taskClient = new TaskClient();
        taskClient.addTask();
    }

}
