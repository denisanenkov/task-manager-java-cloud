package ru.anenkov.tm.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.anenkov.tm.communication.ProjectCommunication;
import ru.anenkov.tm.configuration.MyConfiguration;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.entiity.Project;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectClient {

    private static ApplicationContext context = new AnnotationConfigApplicationContext(MyConfiguration.class);
    private static ProjectCommunication communication = context.getBean("projectCommunication", ProjectCommunication.class);

    public static void main(final String[] args) {
        ProjectClient client = new ProjectClient();
        client.addProject();
    }

    public List<ProjectDTO> getProjectList() {
        List<ProjectDTO> projects = communication.projects();
        System.out.println(projects);
        return projects;
    }

    public ProjectDTO getProjectById() {
        String id = "";
        System.out.print("ENTER PROJECT ID: ");
        id = TerminalUtil.nextLine();
        ProjectDTO project = communication.getProjectById(id);
        System.out.println(project);
        return project;
    }

    public void addProject() {
        ProjectDTO firstProject = new ProjectDTO("first some name", "some description");
        ProjectDTO secondProject = new ProjectDTO("second some name", "some description");
        secondProject.setId(firstProject.getId());
        communication.addProject(firstProject);
        communication.addProject(secondProject);
    }

    public void deleteProject() {
        System.out.print("ENTER PROJECT ID: ");
        String id = TerminalUtil.nextLine();
        communication.deleteProjectById(id);
    }

}
