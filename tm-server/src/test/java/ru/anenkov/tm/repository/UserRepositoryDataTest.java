package ru.anenkov.tm.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.TaskManagerApplication;
import ru.anenkov.tm.model.User;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {TaskManagerApplication.class})
public class UserRepositoryDataTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void findByLoginTest() {
        User user = null;
        Assert.assertNull(user);
        user = userRepository.findByLogin("forTests");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), "forTests");
    }

    @Test
    public void findUserByFirstNameTest() {
        User user = new User("123", "123");
        user.setFirstName("testFirstName");
        userRepository.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userRepository.findAllByFirstName("testFirstName").get(0);
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        Assert.assertEquals("testFirstName", findUser.getFirstName());
        userRepository.deleteById(user.getId());
    }

    @Test
    public void findUserBySecondNameTest() {
        User user = new User("123", passwordEncoder.encode("123"));
        user.setSecondName("testSecondName");
        userRepository.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userRepository.findAllBySecondName("testSecondName").get(0);
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        Assert.assertEquals("testSecondName", findUser.getSecondName());
        userRepository.deleteById(user.getId());
    }

    @Test
    public void findUserByLastNameTest() {
        User user = new User("123", "123");
        user.setLastName("testLastName");
        userRepository.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userRepository.findAllByLastName("testLastName").get(0);
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        Assert.assertEquals("testLastName", findUser.getLastName());
        userRepository.deleteById(user.getId());
    }

    @Test
    public void deleteUserByLoginTest() {
        User user = new User("123", "123");
        userRepository.save(user);
        Assert.assertTrue(userRepository.findByLogin("123") != null);
        userRepository.deleteUserByLogin("123");
        Assert.assertTrue(userRepository.findByLogin("123") == null);
    }

    @Test
    public void saveTest() {
        User user = new User("saveUserTestLogin", "saveUserTestPassword");
        long countBeforeSave = userRepository.count();
        userRepository.save(user);
        long countAfterSave = userRepository.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userRepository.deleteUserByLogin("saveUserTestLogin");
        long countAfterDelete = userRepository.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void findByIdTest() {
        User user = new User("123", "123");
        userRepository.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userRepository.findById(user.getId()).orElse(null);
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        userRepository.deleteById(user.getId());
    }

    @Test
    public void existsByIdTest() {
        User user = new User("123", "123");
        Assert.assertFalse(userRepository.existsById(user.getId()));
        userRepository.save(user);
        Assert.assertTrue(userRepository.existsById(user.getId()));
        userRepository.deleteById(user.getId());
        Assert.assertFalse(userRepository.existsById(user.getId()));
    }

    @Test
    public void countTest() {
        User user = new User("saveUserTestLogin", "saveUserTestPassword");
        long countBeforeSave = userRepository.count();
        userRepository.save(user);
        long countAfterSave = userRepository.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userRepository.deleteUserByLogin("saveUserTestLogin");
        long countAfterDelete = userRepository.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void deleteByIdTest() {
        User user = new User("saveUserTestLogin", "saveUserTestPassword");
        long countBeforeSave = userRepository.count();
        userRepository.save(user);
        long countAfterSave = userRepository.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userRepository.deleteById(user.getId());
        long countAfterDelete = userRepository.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void deleteTest() {
        User user = new User("saveUserTestLogin", "saveUserTestPassword");
        long countBeforeSave = userRepository.count();
        userRepository.save(user);
        long countAfterSave = userRepository.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userRepository.delete(user);
        long countAfterDelete = userRepository.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

}
