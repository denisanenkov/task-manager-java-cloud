package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.repository.UserRepository;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Service
public class UserService implements IUserService {

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Resource
    private AuthenticationManager authenticationManager;

    @Override
    @SneakyThrows
    public boolean login(
            @Nullable @WebParam(name = "username") final String username,
            @Nullable @WebParam(name = "password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication.isAuthenticated();
        } catch (final Exception ex) {
            return false;
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

    @Override
    @SneakyThrows
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByFirstName(@Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyLoginException();
        return findAllByFirstName(firstName).get(0);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserBySecondName(@Nullable final String secondName) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyLoginException();
        return findAllBySecondName(secondName).get(0);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByLastName(@Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLoginException();
        return findAllByLastName(lastName).get(0);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void deleteUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteUserByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public User save(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        return userRepository.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return userRepository.count();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void delete(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        userRepository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.existsById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public @Nullable List<User> findAllByFirstName(@Nullable final String firstName) {
        return userRepository.findAllByFirstName(firstName);
    }

    @Transactional(readOnly = true)
    public @Nullable List<User> findAllBySecondName(@Nullable final String secondName) {
        return userRepository.findAllBySecondName(secondName);
    }

    @Transactional(readOnly = true)
    public @Nullable List<User> findAllByLastName(@Nullable final String lastName) {
        return userRepository.findAllByLastName(lastName);
    }

}
