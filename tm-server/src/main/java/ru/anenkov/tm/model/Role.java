package ru.anenkov.tm.model;

import org.springframework.security.core.GrantedAuthority;
import ru.anenkov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "app_role")
public class Role {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return roleType.name();
    }

}
