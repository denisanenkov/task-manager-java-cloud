package ru.anenkov.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;
import ru.anenkov.tm.model.Task;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_task")
public class TaskDTO extends AbstractEntityDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String name = "";

    @Column
    private String description = "";

    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Column(name = "project_id")
    private String projectId;

    @Column(name = "user_id")
    private String userId;

    public TaskDTO(String name) {
        this.name = name;
    }

    public TaskDTO(String name, String description, Status status, Date dateBegin, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    public TaskDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(String name, String description, String projectId, String userId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TaskDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", dateBegin=" + dateBegin +
                ", dateFinish=" + dateFinish +
                ", projectId='" + projectId + '\'' +
                '}';
    }

    public static TaskDTO toTaskDTO(@NotNull final Task task) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setDateBegin(task.getDateBegin());
        taskDTO.setDateFinish(task.getDateFinish());
        if (task.getProject() != null) taskDTO.setProjectId(task.getProject().getId());
        return taskDTO;
    }

    public static List<TaskDTO> toTaskListDTO(List<Task> taskList) {
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task : taskList) {
            taskDTOList.add(toTaskDTO(task));
        }
        return taskDTOList;
    }

}
