package ru.anenkov.tm.endpoint.rest.unsecured;

import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.api.endpoint.rest.IProjectRestEndpoint;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.service.ProjectService;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import ru.anenkov.tm.dto.ProjectDTO;

import java.util.List;

@RestController
@RequestMapping("/api/free")
public class ProjectRestFreeEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping(value = "/projects")
    public ProjectDTO add(@RequestBody @Nullable ProjectDTO project) {
        projectService.addDTO(project);
        return project;
    }

    @Override
    @PutMapping(value = "/projects")
    public ProjectDTO update(@Nullable @RequestBody ProjectDTO project) {
        projectService.addDTO(project);
        return project;
    }

    @Override
    @GetMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @Nullable ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") final String id) {
        return ProjectDTO.toProjectDTO(projectService.findProjectId(id));
    }

    @Override
    @DeleteMapping(value = "/projects/{id}")
    public String removeOneById(@Nullable @PathVariable final String id) {
        ProjectDTO project = ProjectDTO.toProjectDTO
                (projectService.findProjectId(id));
        if (project == null)
            throw new NoSuchEntitiesException("project with id \"" + id + "\" does not exist!");
        projectService.deleteById(id);
        return "Project with id \"" + id + "\" was deleted successfully!";
    }

    @Override
    @DeleteMapping(value = "/projects")
    public void removeAllProjects() {
        projectService.deleteAll();
    }

    @Override
    @Nullable
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> getListByUserId() {
        return ProjectDTO.toProjectListDTO(projectService.findAllProjects());
    }

    @Override
    @GetMapping(value = "/projects/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return projectService.count();
    }

}
