package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.util.UserUtil;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

/**
 * address: http://localhost:8282/services/TaskEndpoint?wsdl
 * address: http://localhost:8383/services/TaskEndpoint?wsdl
 */

@Component
@WebService
public class TaskEndpoint implements ru.anenkov.tm.api.endpoint.soap.ITaskEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    public void addDTOTask(
            @Nullable @WebParam(name = "task") final TaskDTO task
    ) {
        taskService.addDTO(task);
    }

    @Override
    @WebMethod
    public void addTask(
            @Nullable @WebParam(name = "task") final Task task
    ) {
        taskService.add(task);
    }

    @Override
    @WebMethod
    public void removeAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        taskService.removeAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public List<TaskDTO> findAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return TaskDTO.toTaskListDTO(taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId));
    }

    @Override
    @WebMethod
    public void removeTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "id") final String id
    ) {
        taskService.removeTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id);
    }

    @Override
    @WebMethod
    public TaskDTO findTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "id") final String id
    ) {
        return TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id));
    }

    @Override
    @WebMethod
    public TaskDTO findTaskByUserIdAndProjectIdAndName(
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "name") final String name
    ) {
        return TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, name));
    }

    @Override
    @WebMethod
    public long countByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return taskService.countByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

}
