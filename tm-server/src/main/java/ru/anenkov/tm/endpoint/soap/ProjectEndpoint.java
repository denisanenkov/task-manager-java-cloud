package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.service.ProjectService;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.util.UserUtil;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

/**
 * address: http://localhost:8282/services/ProjectEndpoint?wsdl
 * address: http://localhost:8383/services/ProjectEndpoint?wsdl
 */

@Component
@WebService
public class ProjectEndpoint implements ru.anenkov.tm.api.endpoint.soap.IProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    public void addProject(
            @WebParam(name = "project") ProjectDTO project
    ) {
        projectService.addDTO(project);
    }

    @Override
    @WebMethod
    public ProjectDTO findOneByIdEntity(
            @WebParam(name = "id") @Nullable final String id
    ) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(UserUtil.getUserId(), id));
    }

    @Override
    @WebMethod
    public ProjectDTO findProjectByUserIdAndId(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "id") @Nullable final String id
    ) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(userId, id));
    }

    @Override
    @WebMethod
    public void removeOneById(
            @WebParam(name = "id") @Nullable final String id
    ) {
        projectService.removeProjectByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllProjects() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getList() {
        return ProjectDTO.toProjectListDTO(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @Override
    @WebMethod
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    public ProjectDTO findProjectByUserIdAndName(
            @Nullable @WebParam(name = "name") final String name
    ) {
        return projectService.toProjectDTO(projectService.findProjectByUserIdAndName(UserUtil.getUserId(), name));
    }

}
