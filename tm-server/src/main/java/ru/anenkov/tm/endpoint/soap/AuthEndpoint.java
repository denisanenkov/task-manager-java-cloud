package ru.anenkov.tm.endpoint.soap;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import org.springframework.security.core.Authentication;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.repository.UserRepository;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.User;
import lombok.SneakyThrows;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * address: http://localhost:8282/services/AuthEndpoint?wsdl
 * address: http://localhost:8383/services/AuthEndpoint?wsdl
 */

@Component
@WebService
public class AuthEndpoint implements ru.anenkov.tm.api.endpoint.soap.IAuthEndpoint {

    @Autowired
    private UserRepository userRepository;

    @Resource
    private AuthenticationManager authenticationManager;

    @Override
    @WebMethod
    @SneakyThrows
    public boolean login(
            @Nullable @WebParam(name = "username") final String username,
            @Nullable @WebParam(name = "password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication.isAuthenticated();
        } catch (final Exception ex) {
            return false;
        }
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByLogin(
            @Nullable final @WebParam(name = "login") String login
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByFirstName(
            @Nullable final @WebParam(name = "firstName") String firstName
    ) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByFirstName(firstName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserBySecondName(
            @Nullable final @WebParam(name = "secondName") String secondName
    ) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserBySecondName(secondName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByLastName(
            @Nullable final @WebParam(name = "lastName") String lastName
    ) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByLastName(lastName);
    }

    @Override
    @SneakyThrows
    public void deleteUserByLogin(
            @Nullable final @WebParam(name = "login") String login
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteUserByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User saveUser(
            @Nullable final @WebParam(name = "user") User user
    ) {
        if (user == null) throw new EmptyEntityException();
        return userRepository.save(user);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public long countUsers() {
        return userRepository.count();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteUserById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteUser(
            @Nullable final @WebParam(name = "user") User user
    ) {
        if (user == null) throw new EmptyEntityException();
        userRepository.delete(user);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteAllUsers() {
        userRepository.deleteAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsUserById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.existsById(id);
    }

}
