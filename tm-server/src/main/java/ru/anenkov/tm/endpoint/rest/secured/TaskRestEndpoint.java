package ru.anenkov.tm.endpoint.rest.secured;

import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.api.endpoint.rest.ITaskRestEndpoint;
import org.springframework.web.bind.annotation.*;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.util.UserUtil;
import ru.anenkov.tm.dto.TaskDTO;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> tasks() {
        return TaskDTO.toTaskListDTO(taskService.findAllProjects());
    }

    @GetMapping(value = "/tasks/{id}")
    public TaskDTO getTask(
            @PathVariable @Nullable final String id
    ) {
        TaskDTO task = TaskDTO.toTaskDTO(taskService.findProjectId(id));
        System.out.println("FINDED: " + task);
        if (task == null) throw new NoSuchEntitiesException
                ("Entity \"Task\" with id = " + id + " not found!");
        return task;
    }

    @PostMapping(value = "/tasks")
    public TaskDTO addTask(@RequestBody @Nullable TaskDTO task) {
        taskService.addDTO(task);
        return task;
    }

    @PutMapping(value = "/tasks")
    public TaskDTO updateTask(@RequestBody @Nullable TaskDTO task) {
        taskService.addDTO(task);
        return task;
    }

    @DeleteMapping(value = "/tasks/{id}")
    public String deleteTask(
            @PathVariable @Nullable final String id
    ) {
        taskService.deleteById(id);
        return "Task with id " + id + " was deleted successfully!";
    }

    @DeleteMapping(value = "/tasks")
    public void deleteAllTasks() {
        taskService.deleteAll();
    }

    @GetMapping(value = "/tasks/count")
    public long count() {
        return taskService.count();
    }

}
