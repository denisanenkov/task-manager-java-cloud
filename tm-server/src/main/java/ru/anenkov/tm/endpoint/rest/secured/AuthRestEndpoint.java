package ru.anenkov.tm.endpoint.rest.secured;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.rest.IAuthRestEndpoint;
import ru.anenkov.tm.service.UserService;
import ru.anenkov.tm.model.User;
import lombok.SneakyThrows;

@RestController
@RequestMapping("/api")
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @SneakyThrows
    @GetMapping(value = "/auth/{username}/{password}")
    public boolean login(
            @PathVariable @Nullable final String username,
            @PathVariable @Nullable final String password
    ) {
        return userService.login(username, password);
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/profile")
    public User profile() {
        return userService.profile();
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/logout")
    public void logout() {
        userService.logout();
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/login/{login}")
    public User findByLogin(
            @PathVariable @Nullable final String login
    ) {
        return userService.findByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/firstName/{firstName}")
    public User findUserByFirstName(
            @PathVariable @Nullable final String firstName
    ) {
        return userService.findUserByFirstName(firstName);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/secondName/{secondName}")
    public User findUserBySecondName(
            @PathVariable @Nullable final String secondName
    ) {
        return userService.findUserBySecondName(secondName);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/lastName/{lastName}")
    public User findUserByLastName(
            @PathVariable @Nullable final String lastName
    ) {
        return userService.findUserByLastName(lastName);
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users/login/{login}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteUserByLogin(
            @PathVariable @Nullable final String login
    ) {
        userService.deleteUserByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PostMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User save(
            @RequestBody @Nullable final User user
    ) {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        return userService.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PutMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User update(
            @RequestBody @Nullable final User user
    ) {
        return userService.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User findById(
            @PathVariable @Nullable final String id
    ) {
        return userService.findById(id);
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Iterable<User> findAll() {
        return userService.findAll();
    }

    @Override
    @SneakyThrows
    @GetMapping("/users/count")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public long count() {
        return userService.count();
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteById(
            @PathVariable @Nullable final String id
    ) {
        userService.deleteById(id);
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users/byBody")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(
            @RequestBody @Nullable final User user
    ) {
        userService.delete(user);
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteAll() {
        userService.deleteAll();
    }

    @Override
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PutMapping("/users/updateFirstName/{id}/{newFirstName}")
    public void updateFirstName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newFirstName
    ) {
        @Nullable User user = userService.findById(id);
        user.setFirstName(newFirstName);
        userService.save(user);
    }

    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PutMapping("/users/updateSecondName/{id}/{newSecondName}")
    public void updateSecondName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newSecondName
    ) {
        @Nullable User user = userService.findById(id);
        user.setSecondName(newSecondName);
        userService.save(user);
    }

    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PutMapping("/users/updateLastName/{id}/{newLastName}")
    public void updateLastName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newLastName
    ) {
        @Nullable User user = userService.findById(id);
        user.setLastName(newLastName);
        userService.save(user);
    }

}
