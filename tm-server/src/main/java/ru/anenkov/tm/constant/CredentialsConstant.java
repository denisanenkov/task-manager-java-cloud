package ru.anenkov.tm.constant;

public interface CredentialsConstant {

    String FIRST_USER_LOGIN = "admin";

    String FIRST_USER_PASSWORD = "admin";

    String SECOND_USER_LOGIN = "test";

    String SECOND_USER_PASSWORD = "test";

    String THIRD_USER_LOGIN = "forTests";

    String THIRD_USER_PASSWORD = "forTests";

    String FOURTH_USER_LOGIN = "1";

    String FOURTH_USER_PASSWORD = "1";

}
