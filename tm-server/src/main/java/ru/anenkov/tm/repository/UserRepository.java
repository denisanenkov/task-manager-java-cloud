package ru.anenkov.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.model.User;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    @Nullable
    @Transactional(readOnly = true)
    User findByLogin(@Nullable @Param("login") String login);

    @Nullable
    @Transactional(readOnly = true)
    User findUserByFirstName(@Nullable @Param("first_name") String first_name);

    @Nullable
    @Transactional(readOnly = true)
    User findUserBySecondName(@Nullable @Param("second_name") String second_name);

    @Nullable
    @Transactional(readOnly = true)
    User findUserByLastName(@Nullable @Param("last_name") String last_name);

    @Transactional
    void deleteUserByLogin(@Nullable @Param("login") String login);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllByFirstName(@Nullable @Param("first_name") String first_name);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllBySecondName(@Nullable @Param("second_name") String second_name);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllByLastName(@Nullable @Param("last_name") String last_name);

}
