package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.anenkov.tm.dto.ProjectDTO;

public interface ProjectDtoRepository extends JpaRepository<ProjectDTO, String> {
}
