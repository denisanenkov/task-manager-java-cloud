package ru.anenkov.tm.api.endpoint.soap;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void addDTOTask(
            @Nullable @WebParam(name = "task") TaskDTO task
    );

    @WebMethod
    void addTask(
            @Nullable @WebParam(name = "task") Task task
    );

    @WebMethod
    void removeAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") String projectId
    );

    @WebMethod
    List<TaskDTO> findAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") String projectId
    );

    @WebMethod
    void removeTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "projectId") String projectId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    TaskDTO findTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "projectId") String projectId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    TaskDTO findTaskByUserIdAndProjectIdAndName(
            @Nullable @WebParam(name = "projectId") String projectId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    long countByUserIdAndProjectId(
            @Nullable @WebParam(name = "projectId") String projectId
    );

}
