package ru.anenkov.tm.api.endpoint.rest;

import org.springframework.web.bind.annotation.*;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.User;
import lombok.SneakyThrows;

public interface IAuthRestEndpoint {

    @Nullable
    @SneakyThrows
    @GetMapping("/users")
    Iterable<User> findAll();

    @SneakyThrows
    @DeleteMapping("/users")
    void deleteAll();

    @SneakyThrows
    @GetMapping("/users/count")
    long count();

    @SneakyThrows
    @GetMapping(value = "/logout")
    void logout();

    @SneakyThrows
    @GetMapping(value = "/profile")
    User profile();

    @Nullable
    @SneakyThrows
    @PostMapping("/users")
    User save(@RequestBody @Nullable User user);

    @SneakyThrows
    @DeleteMapping("/users/byBody")
    void delete(@RequestBody @Nullable User user);

    @Nullable
    @SneakyThrows
    @PutMapping("/users")
    User update(@RequestBody @Nullable User user);

    @Nullable
    @SneakyThrows
    @GetMapping("/users/{id}")
    User findById(@PathVariable @Nullable String id);

    @SneakyThrows
    @DeleteMapping("/users/{id}")
    void deleteById(@PathVariable @Nullable String id);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/login/{login}")
    User findByLogin(@PathVariable @Nullable String login);

    @SneakyThrows
    @DeleteMapping("/users/login/{login}")
    void deleteUserByLogin(@PathVariable @Nullable String login);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/lastName/{lastName}")
    User findUserByLastName(@PathVariable @Nullable String lastName);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/firstName/{firstName}")
    User findUserByFirstName(@PathVariable @Nullable String firstName);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/secondName/{secondName}")
    User findUserBySecondName(@PathVariable @Nullable String secondName);

    @SneakyThrows
    @GetMapping(value = "/auth/{username}/{password}")
    boolean login(@PathVariable @Nullable String username, @PathVariable @Nullable String password);

    @SneakyThrows
    @PutMapping("/users/updateFirstName/{id}/{newFirstName}")
    void updateFirstName(@PathVariable @Nullable String id, @PathVariable @Nullable String newFirstName);

}
