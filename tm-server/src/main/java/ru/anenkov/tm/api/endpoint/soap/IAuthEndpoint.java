package ru.anenkov.tm.api.endpoint.soap;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.User;
import lombok.SneakyThrows;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {

    @Nullable
    @WebMethod
    @SneakyThrows
    User profile();

    @WebMethod
    @SneakyThrows
    boolean logout();

    @WebMethod
    @SneakyThrows
    long countUsers();

    @WebMethod
    @SneakyThrows
    void deleteAllUsers();

    @Nullable
    @WebMethod
    @SneakyThrows
    User saveUser(@Nullable @WebParam(name = "user") User user);

    @Nullable
    @WebMethod
    @SneakyThrows
    User findUserById(@Nullable @WebParam(name = "id") String id);

    @WebMethod
    @SneakyThrows
    void deleteUser(@Nullable @WebParam(name = "user") User user);

    @WebMethod
    @SneakyThrows
    void deleteUserById(@Nullable @WebParam(name = "id") String id);

    @WebMethod
    @SneakyThrows
    boolean existsUserById(@Nullable @WebParam(name = "id") String id);

    @Nullable
    @WebMethod
    @SneakyThrows
    User findUserByLogin(@Nullable @WebParam(name = "login") String login);

    @SneakyThrows
    void deleteUserByLogin(@Nullable @WebParam(name = "login") String login);

    @Nullable
    @WebMethod
    @SneakyThrows
    User findUserByLastName(@Nullable @WebParam(name = "lastName") String lastName);

    @Nullable
    @WebMethod
    @SneakyThrows
    User findUserByFirstName(@Nullable @WebParam(name = "firstName") String firstName);

    @Nullable
    @WebMethod
    @SneakyThrows
    User findUserBySecondName(@Nullable @WebParam(name = "secondName") String secondName);

    @WebMethod
    @SneakyThrows
    boolean login(@Nullable @WebParam(name = "username") String username, @Nullable @WebParam(name = "password") String password);

}
